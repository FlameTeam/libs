#ifndef FLAMEIDE_TEMPLATES_ALLOCATOR_HPP
#define FLAMEIDE_TEMPLATES_ALLOCATOR_HPP

#include <FlameIDE/Templates/Allocator/Interface.hpp>
#include <FlameIDE/Templates/Allocator/MallocAllocator.hpp>
#include <FlameIDE/Templates/Allocator/ObjectAllocator.hpp>
#include <FlameIDE/Templates/Allocator/ArrayAllocator.hpp>

#endif // FLAMEIDE_TEMPLATES_ALLOCATOR_HPP
